clean:
	rm bin/add
	rm bin/addcu

all:
	make add
	make addcu

add:
	g++ add.cpp -o bin/add

addcu:
	nvcc add.cu -o bin/addcu

test:
	time ./bin/add
	nvprof ./bin/addcu
