#include <iostream>
#include <math.h>
#include <time.h>

#define iters 1000

__global__
void add(uint n, float *x, float *y) {
  int index = threadIdx.x;
  int stride = blockDim.x;

  for(uint i = index; i < n; i += stride) {
    y[i] = x[i] + y[i];
  }
}

int main(void) {
  int N = 1<<20;

  float *x;
  float *y;

  cudaMallocManaged(&x, N*sizeof(float));
  cudaMallocManaged(&y, N*sizeof(float));

  for(uint i = 0; i < N; i++) {
    x[i] = 1.0f;
    y[i] = 1.0f;
  }

  clock_t begin = clock();
  for(int j = 0; j < iters; j++) {
    add<<<1, 1024>>>(N,x,y);
  }
  clock_t end = clock();


  cudaDeviceSynchronize();
  std::cout << y[1] << '\n';

  cudaFree(x);
  cudaFree(y);

  std::cout << "Took: " << (double)(end - begin)/CLOCKS_PER_SEC*1000*1000 << "us \n";
  std::cout << "Added " << N << " Numbers" << '\n';

  cudaDeviceReset();
  return 0;

}
