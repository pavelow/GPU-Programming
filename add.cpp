#include <iostream>
#include <math.h>
#include <time.h>

#define iters 1000

void add(uint n, float *x, float *y) {
  for(uint i = 0; i <n; i++) {
    y[i] = x[i] + y[i];
  }
}

int main(void) {
  uint N = 1<<20;

  float *x = new float[N];
  float *y = new float[N];

  for(uint i = 0; i < N; i++) {
    x[i] = 1.0f;
    y[i] = 1.0f;
  }

  clock_t begin = clock();
  for(int j = 0; j < iters; j++) {
    add(N,x,y);
  }
  clock_t end = clock();


  std::cout << "Added " << N << " Numbers" << '\n';
  std::cout << "Took: " << (double)(end - begin)/CLOCKS_PER_SEC*1000*1000 << "us \n";
  delete [] x;
  delete [] y;

  return 0;
}
